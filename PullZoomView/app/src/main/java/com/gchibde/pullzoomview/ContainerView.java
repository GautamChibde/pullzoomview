package com.gchibde.pullzoomview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ContainerView extends FrameLayout implements AbsListView.OnScrollListener {
    private ListAdapter adapter;
    private ImageView mHeader;
    private ListView itemList;
    protected LayoutInflater inflater;
    private TextView title;
    private FrameLayout frame;
    private int mHeaderHeight;
    private ScalingRunnable scalingRunnable;
    float mLastMotionY = -1.0F;
    float mLastScale = -1.0F;
    float mMaxScale = -1.0F;
    int mActivePointerId = -1;
    private int mScreenHeight;
    float ratio = 1;
    private static final Interpolator sInterpolator = new Interpolator() {
        public float getInterpolation(float paramAnonymousFloat) {
            float f = paramAnonymousFloat - 1.0F;
            return 1.0F + f * (f * (f * (f * f)));
        }
    };

    private int mMinHeaderTranslation;
    private View mPlaceHolderView;

    public ContainerView(Context context) {
        super(context);
    }

    public ContainerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
    }

    private void init(Context context) {
        mHeader = new ImageView(context);
        itemList = new ListView(context);
        title = new TextView(context);
        frame = new FrameLayout(context);
        DisplayMetrics localDisplayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay()
                .getMetrics(localDisplayMetrics);
        int i = localDisplayMetrics.widthPixels;
        this.mScreenHeight = localDisplayMetrics.heightPixels;

        mMinHeaderTranslation = -getResources().getDimensionPixelSize(R.dimen.header_height)
                + getResources().getDimensionPixelSize(R.dimen.action_bar);
        mPlaceHolderView = inflater.inflate(R.layout.view_header_placeholder, itemList, false);
        initTitle();
        initFrame();
        initList();
        this.scalingRunnable = new ScalingRunnable();
        setHeaderViewSize(i, (int) (9.0F * (i / 16.0F)));
        addView(itemList);
        addView(frame);
    }

    private void initList() {
        if (adapter != null) {
            itemList.setAdapter(adapter);
        } else {
            throw new NullPointerException("adapter not set use itemList.setAdapter()");
        }
        itemList.addHeaderView(mPlaceHolderView);
        itemList.setOnScrollListener(this);
        itemList.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent paramMotionEvent) {
                return touchEvent(paramMotionEvent);
            }
        });

    }

    private void initFrame() {
        frame.addView(mHeader);
        frame.addView(title);
    }

    private void initTitle() {
        title.setText(String.valueOf("Sample"));
        title.setTextSize(23);
        title.setTextColor(Color.WHITE);
        title.setGravity(Gravity.BOTTOM);
        title.setPadding(8, 8, 8, 8);
    }

    private boolean touchEvent(MotionEvent paramMotionEvent) {
        Log.d("onTouchEvent", "" + (0xFF & paramMotionEvent.getAction()));
        if (startZoom()) {
            switch (0xFF & paramMotionEvent.getAction()) {
                case 0:
                    if (!scalingRunnable.mIsFinished) {
                        scalingRunnable.abortAnimation();
                    }
                    mLastMotionY = paramMotionEvent.getY();
                    mActivePointerId = paramMotionEvent.getPointerId(0);
                    mMaxScale = (mScreenHeight / mHeaderHeight);
                    mLastScale = (frame.getBottom() / mHeaderHeight);
                    break;
                case 2:
                    int j = paramMotionEvent.findPointerIndex(mActivePointerId);
                    if (j == -1) {
                        Log.e("PullToZoomListView", "Invalid pointerId="
                                + mActivePointerId + " in onTouchEvent");
                    } else {
                        if (mLastMotionY == -1.0F)
                            mLastMotionY = paramMotionEvent.getY(j);
                        if (frame.getBottom() >= mHeaderHeight) {
                            ViewGroup.LayoutParams localLayoutParams = frame
                                    .getLayoutParams();
                            float f = ((paramMotionEvent.getY(j) - mLastMotionY + frame
                                    .getBottom()) / mHeaderHeight - mLastScale)
                                    / 2.0F + mLastScale;
                            if ((mLastScale <= 1.0D) && (f < mLastScale)) {
                                localLayoutParams.height = mHeaderHeight;
                                frame
                                        .setLayoutParams(localLayoutParams);
                                return onTouchEvent(paramMotionEvent);
                            }
                            mLastScale = Math.min(Math.max(f, 1.0F),
                                    mMaxScale);
                            localLayoutParams.height = ((int) (mHeaderHeight * mLastScale));
                            if (localLayoutParams.height < mScreenHeight)
                                frame
                                        .setLayoutParams(localLayoutParams);
                            mLastMotionY = paramMotionEvent.getY(j);
                            return true;
                        }
                        mLastMotionY = paramMotionEvent.getY(j);
                    }
                    break;
                case 1:
                    reset();
                    endScraling();
                    break;
                case 3:
                    int i = paramMotionEvent.getActionIndex();
                    mLastMotionY = paramMotionEvent.getY(i);
                    mActivePointerId = paramMotionEvent.getPointerId(i);
                    break;
                case 5:
                    onSecondaryPointerUp(paramMotionEvent);
                    mLastMotionY = paramMotionEvent.getY(paramMotionEvent
                            .findPointerIndex(mActivePointerId));
                    break;
            }
        }
        return false;
    }

    private void endScraling() {
        if (this.frame.getBottom() >= this.mHeaderHeight)
            this.scalingRunnable.startAnimation(200L);
    }

    private void onSecondaryPointerUp(MotionEvent paramMotionEvent) {
        int i = (paramMotionEvent.getAction()) >> 8;
        if (paramMotionEvent.getPointerId(i) == this.mActivePointerId)
            if (i != 0) {
                this.mLastMotionY = paramMotionEvent.getY(0);
                this.mActivePointerId = paramMotionEvent.getPointerId(0);
            }
    }

    private void reset() {
        this.mActivePointerId = -1;
        this.mLastMotionY = -1.0F;
        this.mMaxScale = -1.0F;
        this.mLastScale = -1.0F;
    }

    public void setHeaderViewSize(int paramInt1, int paramInt2) {
        Object localObject = this.frame.getLayoutParams();
        if (localObject == null)
            localObject = new AbsListView.LayoutParams(paramInt1, paramInt2);
        ((ViewGroup.LayoutParams) localObject).width = paramInt1;
        ((ViewGroup.LayoutParams) localObject).height = paramInt2;
        this.frame
                .setLayoutParams((ViewGroup.LayoutParams) localObject);
        this.mHeaderHeight = paramInt2;
    }

    private boolean startZoom() {
        return mHeaderHeight <= mPlaceHolderView.getBottom();
    }

    @Override
    public void onScroll(AbsListView absListView, int para1, int i1, int i2) {
        int scrollY = getScroll_Y();
        ratio = clamp(mHeader.getTranslationY() / mMinHeaderTranslation, 0.0f, 1.0f);
        mHeader.setTranslationY(Math.max(-scrollY, mMinHeaderTranslation));
        title.setTranslationY(Math.max(-scrollY, mMinHeaderTranslation));

    }

    public static float clamp(float value, float min, float max) {
        return Math.max(min, Math.min(value, max));
    }

    public int getScroll_Y() {
        View c = itemList.getChildAt(0);
        if (c == null) {
            return 0;
        }
        int firstVisiblePosition = itemList.getFirstVisiblePosition();
        int top = c.getTop();
        int headerHeight = 0;
        if (firstVisiblePosition >= 1) {
            headerHeight = mPlaceHolderView.getHeight();
        }
        return -top + firstVisiblePosition * c.getHeight() + headerHeight;
    }

    public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {

    }

    class ScalingRunnable implements Runnable {
        long mDuration;
        boolean mIsFinished = true;
        float mScale;
        long mStartTime;

        ScalingRunnable() {
        }

        public void abortAnimation() {
            this.mIsFinished = true;
        }

        public boolean isFinished() {
            return this.mIsFinished;
        }

        public void run() {
            float f2;
            ViewGroup.LayoutParams localLayoutParams;
            if ((!this.mIsFinished) && (this.mScale > 1.0D)) {
                float f1 = ((float) SystemClock.currentThreadTimeMillis() - (float) this.mStartTime)
                        / (float) this.mDuration;
                f2 = this.mScale - (this.mScale - 1.0F)
                        * ContainerView.sInterpolator.getInterpolation(f1);
                localLayoutParams = ContainerView.this.frame
                        .getLayoutParams();
                if (f2 > 1.0F) {
                    Log.d("mmm", "f2>1.0");
                    localLayoutParams.height = ContainerView.this.mHeaderHeight;
                    localLayoutParams.height = ((int) (f2 * ContainerView.this.mHeaderHeight));
                    ContainerView.this.frame
                            .setLayoutParams(localLayoutParams);
                    ContainerView.this.post(this);
                    return;
                }
                this.mIsFinished = true;
            }
        }

        public void startAnimation(long paramLong) {
            this.mStartTime = SystemClock.currentThreadTimeMillis();
            this.mDuration = paramLong;
            this.mScale = ((float) (ContainerView.this.frame
                    .getBottom()) / ContainerView.this.mHeaderHeight);
            this.mIsFinished = false;
            ContainerView.this.post(this);
        }
    }

    public TextView getTitle() {
        return this.title;
    }

    public void setAdapter(ListAdapter adapter) {
        this.adapter = adapter;
        init(getContext());
    }

    public ImageView getHeaderImage() {
        return this.mHeader;
    }
}
