package com.gchibde.pullzoomview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    ContainerView containerView;
    View v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        containerView = (ContainerView) findViewById(R.id.listview) ;
        String[] adapterData = new String[]{"Activity", "Service", "Content Provider", "Intent",
                "BroadcastReceiver", "ADT", "Sqlite3", "HttpClient", "DDMS", "Android Studio",
                "Fragment", "Loader", "test1", "test1", "test1", "test1", "test1", "test1",
                "test1", "test1", "test1", "test1", "test1", "test1", "test1"};
        containerView.setAdapter(new ArrayAdapter<>(MainActivity.this,
                android.R.layout.simple_list_item_1, adapterData));
        containerView.getHeaderImage().setScaleType(ImageView.ScaleType.CENTER_CROP);
        containerView.getHeaderImage().setImageResource(R.drawable.splash01);
        containerView.getTitle().setText("New Text");
        containerView.getTitle().setTextSize(20);
    }
}
